if (process.argv.length === 2) {
    console.error('Expected at least one argument!');
    process.exit(1);
  }

var filepath = 'orig_data.csv';//process.argv[2];

const fs = require("fs");
const csvParser = require("csv-parser");

  
const UpdatedaStream = fs.createWriteStream("cleaned_" + filepath);
const result = [];

fs.createReadStream(filepath)
  .pipe(csvParser())
  .on("data", (data) => {
    if (data["account_language"] == "en") {
        console.log(" english user");
        var profileDescription = data["user_profile_description"];
        profileDescription = profileDescription.replace(/[^a-zA-Z0-9# ]/g, '');
        data["user_profile_description"] = profileDescription;
        result.push(data);
    }
  })
  .on("end", () => {
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    const csvWriter = createCsvWriter({
        path: "cleaned_" + filepath,
        header: [
          {id: 'userid', title: 'userid'},
          {id: 'user_display_name', title: 'user_display_name'},
          {id: 'user_screen_name', title: 'user_screen_name'},
          {id: 'user_reported_location', title: 'user_reported_location'},
          {id: 'user_profile_description', title: 'user_profile_description'},
          {id: 'user_profile_url', title: 'user_profile_url'},
          {id: 'follower_count', title: 'follower_count'},
          {id: 'following_count', title: 'following_count'},
          {id: 'account_creation_date', title: 'account_creation_date'},
          {id: 'account_language', title: 'account_language'}
        ]
      });
    console.log(result.length)
      csvWriter
      .writeRecords(result)
      .then(()=> console.log('The CSV file was written successfully'));
  });


 